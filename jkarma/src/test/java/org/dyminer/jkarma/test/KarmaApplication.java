package org.dyminer.jkarma.test;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.dyminer.jkarma.Detectors;
import org.dyminer.jkarma.KarmaArgs;
import org.dyminer.jkarma.KarmaDetector;
import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.ExponentialKernel;
import org.dyminer.jkarma.datastreams.FadingFunction;
import org.dyminer.jkarma.datastreams.InverseLinearKernel;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.jkarma.datastreams.UniformKernel;
import org.dyminer.jkarma.events.ChangeDetectedEvent;
import org.dyminer.jkarma.events.ChangeDetectorEventListener;
import org.dyminer.jkarma.events.ChangeNotDetectedEvent;
import org.dyminer.jkarma.events.DataStreamEventListener;
import org.dyminer.jkarma.events.DataStreamExhaustedEvent;
import org.dyminer.jkarma.events.DataStreamStartedEvent;
import org.dyminer.jkarma.events.DetectionCompletedEvent;
import org.dyminer.jkarma.events.DetectionStartedEvent;
import org.dyminer.jkarma.events.KarmaEventListener;
import org.dyminer.jkarma.events.LatticeUpdateCompletedEvent;
import org.dyminer.jkarma.events.LatticeUpdateStartedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeAcceptedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeDetectionCompletedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeDetectionStartedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeNotAcceptedEvent;
import org.dyminer.jkarma.events.TransactionArrivedEvent;
import org.dyminer.jkarma.events.TransactionBlockArrivedEvent;
import org.dyminer.jminer.events.ItemSetAddedEvent;
import org.dyminer.jminer.events.ItemSetAddedRegisteredEvent;
import org.dyminer.jminer.events.ItemSetEventListener;
import org.dyminer.jminer.events.ItemSetRemovedEvent;
import org.dyminer.jminer.events.ItemSetRemovedRegisteredEvent;
import org.dyminer.jminer.events.ItemSetUpdatedEvent;
import org.dyminer.jminer.joiners.EdgeLexicographicJoiner;
import org.dyminer.jminer.joiners.FrequencyEvaluator;
import org.dyminer.jminer.joiners.Joiner;
import org.dyminer.jminer.joiners.TidlistJoiner;
import org.dyminer.jminer.providers.TidlistProvider;
import org.dyminer.jminer.providers.VerticalDataProvider;
import org.dyminer.jminer.structures.Pair;
import org.dyminer.jstreamer.CsvBeanSource;
import org.dyminer.model.LabeledEdge;
import org.dyminer.model.TemporalEdge;
import org.dyminer.model.TemporalGraph;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

public class KarmaApplication implements Runnable{
	
	private KarmaArgs arguments;
	private InputStreamReader inputReader;
	private Writer fileWriter;
	private XMLStreamWriter xml;
	protected long totalLatticeUpdateTime = 0;
	protected long totalChangeSearchTime = 0;
	protected long changePointCount = 0;
	protected long blockCount = 0;
	protected long totalFPCount = 0;
	protected long totalIFPCount = 0;
	protected long totalJEPCount = 0;
	protected long totalAcceptedEPCount = 0;
	protected long totalNotAcceptedEPCount = 0;
	protected float totalMacroChange = 0;
	protected float totalAcceptedEPSupport = 0;
	protected float totalAcceptedEPGrowthRate = 0;
	protected float totalNotAcceptedEPSupport = 0;
	protected long latestLatticeUpdateTime;
	protected long latestFPCount;
	protected long latestIFPCount;
	protected float latestMacroChange;
	protected boolean macroChangeEncountered;
	protected int latestIFPCout;
	
	public KarmaApplication(KarmaArgs arguments) throws XMLStreamException, FactoryConfigurationError, IOException {
		if(arguments==null) {
			throw new NullPointerException();
		}
		
		this.arguments = arguments;
		//we open the input stream reader
		this.inputReader = new InputStreamReader(new FileInputStream(this.arguments.getStreamSource()), StandardCharsets.UTF_8);
		//we open the output xml writer
		this.fileWriter = new FileWriter(this.arguments.getOutputFile());
		this.xml = XMLOutputFactory.newInstance().createXMLStreamWriter(this.fileWriter);
	}


	@Override
	public void run() {
		//we open the data source
		CsvBeanSource<TemporalEdge> dataSource = new CsvBeanSource<TemporalEdge>(this.inputReader, TemporalEdge.class);
		Stream<TemporalEdge> dataStream = dataSource.stream();

		//we pass the data source to the algorithm instance (and we run it)
		VerticalDataProvider<LabeledEdge, Set<Integer>> provider = new TidlistProvider<>(true);
		Joiner<LabeledEdge> joiner = new EdgeLexicographicJoiner();
		FrequencyEvaluator<Set<Integer>> evaluator = new TidlistJoiner(this.arguments.getMinimumSupport());
		
		//we build the proper fading function
		FadingFunction fader = null;
		if(arguments.isUsingExponentialFader()) {
			fader = new ExponentialKernel(arguments.getMinimumMacrochange(), arguments.getTheta());
		}else if(arguments.isUsingLinearFader()) {
			fader = new InverseLinearKernel(arguments.getMinimumMacrochange(), arguments.getTheta());
		}else {
			fader = new UniformKernel(arguments.getMinimumMacrochange());
		}

		/*
		 * This is a preprocessing step on the stream.
		 * More precisely, we convert the stream of temporal edges 
		 * to a stream of temporal graphs (only in subgraph karma).
		 */
		Stream<TemporalGraph> graphStream = dataStream.map(new Function<TemporalEdge,TemporalGraph>(){
			
			private Instant lastTimestamp;
			private int transactionId = 0;
			private HashSet<LabeledEdge> edges;

			@Override
			public TemporalGraph apply(TemporalEdge edge) {
				Instant edgeInstant = edge.timestamp.toInstant();
				TemporalGraph snapshot = null;

				if(this.lastTimestamp==null || this.lastTimestamp.isBefore(edgeInstant)){
					if(this.lastTimestamp!=null) {
						snapshot = new TemporalGraph(this.transactionId, this.lastTimestamp, this.edges);
					}
					this.edges = new HashSet<>();
					this.transactionId++;
					this.lastTimestamp = edgeInstant;
				}

				this.edges.add(edge);
				return snapshot;
			}

		}).filter(graph -> graph!=null);

		//we instantiate the stream detector
		KarmaDetector<TemporalGraph, LabeledEdge, Set<Integer>> detector = Detectors.subgraphKarma(
			this.arguments, graphStream, joiner, provider, evaluator, fader
		);

		/*
		 * we listen for change detection related event
		 */
		detector.registerListener(
			new ChangeDetectorEventListener() {

				private DecimalFormat df = new DecimalFormat("#.####");
				private String mcMin = df.format(arguments.getMinimumMacrochange() * 100);
				private long duration;

				@Override
				public void detectionStarted(DetectionStartedEvent event) {
					TimeWindow landmark = event.getLandmarkWindow();
					Block<?> block = event.getLatestBlock();
					duration = System.currentTimeMillis();
					try {
						xml.writeStartElement("changes");
						xml.writeAttribute("in", landmark.toString());
						xml.writeAttribute("respect", landmark.getFirstInstant()+", "+block.getLastInstant());
					}catch(XMLStreamException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void detectionCompleted(DetectionCompletedEvent event) {
					totalChangeSearchTime += System.currentTimeMillis()-duration;
					try {
						xml.writeEndElement();
					}catch(XMLStreamException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void changeDetected(ChangeDetectedEvent event) {
					String percentage = this.df.format(event.getChangeAmount() * 100);
					System.err.println("Macroscopic Change detected ("+
						percentage+"% over "+mcMin+"% min)");
					
					changePointCount++;
					macroChangeEncountered = true;
					latestMacroChange = event.getChangeAmount();
					totalMacroChange += event.getChangeAmount();
					
					//we add up the count only when triggering
					totalFPCount += latestFPCount;
					totalIFPCount += latestIFPCount;
					try {
						xml.writeAttribute("macroChange", percentage);
					}catch(XMLStreamException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void changeNotDetected(ChangeNotDetectedEvent event) {
					String percentage = this.df.format(event.getChangeAmount() * 100);
					
					macroChangeEncountered = false;
					try {
						xml.writeAttribute("macroChange", percentage);
					}catch(XMLStreamException e) {
						e.printStackTrace();
					}
				}

			}
		);

		/*
		 * we listen for data stream related events
		 */
		detector.registerListener(
			new DataStreamEventListener<TemporalGraph>() {

				private long startTime;

				@Override
				public void transactionArrived(TransactionArrivedEvent<TemporalGraph> event) {
					
				}

				@Override
				public void transactionBlockArrived(TransactionBlockArrivedEvent<TemporalGraph> event) {
					System.out.println("block arrived: "+event.getBlock());
					blockCount++;
					macroChangeEncountered = false;
					latestFPCount = 0;
					latestIFPCout = 0;
				}

				@Override
				public void dataStreamStarted(DataStreamStartedEvent<TemporalGraph> event) {
					System.out.println("stream started");
					try {
						//we open the root element
						xml.writeStartDocument();
						xml.writeStartElement("log");
						xml.writeStartElement("params");
						
						//we write the input parameters
						xml.writeStartElement("dataParams");
						xml.writeEmptyElement("filepath");
						xml.writeAttribute("value", arguments.getStreamSource().getAbsolutePath());
						xml.writeEndElement();

						//we write the data stream parameters
						xml.writeStartElement("streamParams");
						xml.writeEmptyElement("batchSize");
						xml.writeAttribute("value", Integer.toString(arguments.getBatchSize()));
						xml.writeEmptyElement("blockSize");
						xml.writeAttribute("value", Integer.toString(arguments.getBlockSize()));
						xml.writeEndElement();

						//we write the mining parameters
						xml.writeStartElement("miningParams");
						xml.writeEmptyElement("minimumSupport");
						xml.writeAttribute("value", Float.toString(arguments.getMinimumSupport()));
						xml.writeEmptyElement("minimumMacroChange");
						xml.writeAttribute("value", Float.toString(arguments.getMinimumMacrochange()));
						xml.writeEmptyElement("minimumGrowthRate");
						xml.writeAttribute("value", Float.toString(arguments.getMinimumGrowthRate()));
						xml.writeEmptyElement("maximumPatternLength");
						xml.writeAttribute("value", Float.toString(arguments.getMaximumDepth()));
						xml.writeEmptyElement("microscopicFromClosed");
						xml.writeAttribute("value", Boolean.toString(arguments.isMiningClosed()));
						xml.writeEmptyElement("fadingFunction");
						if(arguments.isUsingExponentialFader()) {
							xml.writeAttribute("type", "exponential");
							xml.writeAttribute("theta", Float.toString(arguments.getTheta()));
						}else if(arguments.isUsingLinearFader()){
							xml.writeAttribute("type", "linear");
							xml.writeAttribute("theta", Float.toString(arguments.getTheta()));
						}else {
							xml.writeAttribute("type", "uniform");
						}
						
						xml.writeEndElement();

						//we close the "params" element
						xml.writeEndElement();
						
						//and we begin the analysis
						xml.writeStartElement("results");
						xml.flush();
					} catch (XMLStreamException e) {
						e.printStackTrace();
					}
					
					this.startTime = System.currentTimeMillis();
				}

				
				
				@Override
				public void dataStreamExhausted(DataStreamExhaustedEvent<TemporalGraph> event) {
					long duration = System.currentTimeMillis()-startTime;
					long seconds = (duration) / 1000;
					System.out.println("stream exhausted in "+seconds+" (s)");
									
					try {
						//average values with respect to the number of change points spotted
						float avgFchange = totalMacroChange/(float)changePointCount;
						float avgFPCount = totalFPCount/(float)changePointCount;
						float avgIFPCount = totalIFPCount/(float)changePointCount;
						float avgAcceptedEPCount = (float)totalAcceptedEPCount/(float)changePointCount;
						float avgRejectedEPCount = (float)totalNotAcceptedEPCount/(float)changePointCount;
						
						float onlyAcceptedEPS = (float)(totalAcceptedEPCount - totalJEPCount);
						float avgAcceptedEPSupport = (float)totalAcceptedEPSupport/onlyAcceptedEPS;
						float avgAcceptedEPGrowthRate = (float)totalAcceptedEPGrowthRate/onlyAcceptedEPS;
						float avgRejectedEPSupport = (float)totalNotAcceptedEPSupport/(float)totalNotAcceptedEPCount;
						
						xml.writeEndElement();
						xml.writeStartElement("stats");
						xml.writeStartElement("totalTime");
						xml.writeAttribute("ms", Long.toString(duration));
						xml.writeEndElement();
						xml.writeStartElement("latticeUpdateTime");
						xml.writeAttribute("ms", Long.toString(totalLatticeUpdateTime));
						xml.writeEndElement();
						xml.writeStartElement("changeSearchTime");
						xml.writeAttribute("ms", Long.toString(totalChangeSearchTime));
						xml.writeEndElement();
						xml.writeStartElement("changePointCount");
						xml.writeAttribute("value", Long.toString(changePointCount));
						xml.writeEndElement();
						xml.writeStartElement("blockCount");
						xml.writeAttribute("value", Long.toString(blockCount));
						xml.writeEndElement();
						xml.writeStartElement("avgMacroChange");
						xml.writeAttribute("value", Float.toString(avgFchange));
						xml.writeEndElement();
						xml.writeStartElement("avgFPCount");
						xml.writeAttribute("value", Float.toString(avgFPCount));
						xml.writeEndElement();
						xml.writeStartElement("avgIFPCount");
						xml.writeAttribute("value", Float.toString(avgIFPCount));
						xml.writeEndElement();
						xml.writeStartElement("avgAcceptedEPCount");
						xml.writeAttribute("value", Float.toString(avgAcceptedEPCount));
						xml.writeEndElement();
						xml.writeStartElement("avgAcceptedEPGrowthRate");
						xml.writeAttribute("value", Float.toString(avgAcceptedEPGrowthRate));
						xml.writeEndElement();
						xml.writeStartElement("avgAcceptedEPSupport");
						xml.writeAttribute("value", Float.toString(avgAcceptedEPSupport));
						xml.writeEndElement();
						xml.writeStartElement("avgNotAcceptedEPCount");
						xml.writeAttribute("value", Float.toString(avgRejectedEPCount));
						xml.writeEndElement();
						xml.writeStartElement("avgNotAcceptedEPSupport");
						xml.writeAttribute("value", Float.toString(avgRejectedEPSupport));
						xml.writeEndElement();
						xml.writeStartElement("totalFPCount");
						xml.writeAttribute("value", Long.toString(totalFPCount));
						xml.writeEndElement();
						xml.writeStartElement("totalIFPCount");
						xml.writeAttribute("value", Long.toString(totalIFPCount));
						xml.writeEndElement();
						xml.writeStartElement("jumpingEPCount");
						xml.writeAttribute("value", Long.toString(totalJEPCount));
						xml.writeEndElement();
						xml.writeStartElement("acceptedEPCount");
						xml.writeAttribute("value", Long.toString(totalAcceptedEPCount));
						xml.writeEndElement();
						xml.writeStartElement("rejectedEPCount");
						xml.writeAttribute("value", Long.toString(totalNotAcceptedEPCount));
						xml.writeEndElement();
						xml.writeEndElement();
						xml.writeEndElement();
						xml.flush();
						xml.close();
						fileWriter.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (XMLStreamException e1) {
						e1.printStackTrace();
					}
				}
			}
		);


		/*
		 * we listen to specific karma related events
		 */
		detector.registerListener(
			new KarmaEventListener<LabeledEdge, Pair<Set<Integer>>>(){

				private DecimalFormat df = new DecimalFormat("#.####");
				private String grMin = df.format(arguments.getMinimumGrowthRate() * 100);

				@Override
				public void latticeUpdateStarted(LatticeUpdateStartedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					latestLatticeUpdateTime = System.currentTimeMillis();
				}

				@Override
				public void latticeUpdateCompleted(
					LatticeUpdateCompletedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					latestLatticeUpdateTime = System.currentTimeMillis()-latestLatticeUpdateTime;
					totalLatticeUpdateTime += latestLatticeUpdateTime;
				}

				@Override
				public void microscopicChangeDetectionStarted(
					MicroscopicChangeDetectionStartedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//do nothing
				}

				@Override
				public void microscopicChangeAccepted(
					MicroscopicChangeAcceptedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					float gr = event.getGrowthRate();
					String percentage = this.df.format(gr * 100);
					String pastSupport = this.df.format(event.getPastRelativeFrequency() * 100);
					String updatedSupport = this.df.format(event.getUpdatedRelativeFrequency() * 100);
					
					totalAcceptedEPCount++;
					if(Float.isInfinite(gr)){
						totalJEPCount++;
					}else {
						totalAcceptedEPGrowthRate+=gr;
						totalAcceptedEPSupport+=event.getUpdatedRelativeFrequency();
					}
					
					try {
						xml.writeStartElement("pattern");
						xml.writeAttribute("growthRate", percentage);
						xml.writeAttribute("pastSupport", pastSupport);
						xml.writeAttribute("updatedSupport", updatedSupport);
						//xml.writeAttribute("support", Float.toString(support));
						xml.writeStartElement("value");
						xml.writeCharacters(event.getPattern().toString());
						xml.writeEndElement();
						xml.writeStartElement("evaluationOnLandmark");
						xml.writeAttribute("window",event.getLandmarkWindow().toString());
						xml.writeCharacters(event.getPattern().getEval().getAggregate().toString());
						xml.writeEndElement();
						xml.writeStartElement("evaluationOnBlock");
						xml.writeAttribute("window",event.getLatestBlock().toString());
						xml.writeCharacters(event.getPattern().getEval().getIncrement().toString());
						xml.writeEndElement();
						xml.writeEndElement();
					} catch (XMLStreamException e1) {
						e1.printStackTrace();
					}
				}

				@Override
				public void microscopicChangeNotAccepted(
					MicroscopicChangeNotAcceptedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					totalNotAcceptedEPCount++;
					totalNotAcceptedEPSupport+=event.getUpdatedRelativeFrequency();
				}

				@Override
				public void microscopicChangeDetectionCompleted(
					MicroscopicChangeDetectionCompletedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//do nothing
				}				
			}
		);



		/*
		 * we listen to specific karma itemset related events
		 */
		detector.registerListener(
			new ItemSetEventListener<LabeledEdge, Pair<Set<Integer>>>(){

				@Override
				public void itemsetAdded(ItemSetAddedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//System.out.println(event.getItemSet()+" eval:"+event.getItemSet().getEval());
					if(!macroChangeEncountered) {
						latestFPCount++;
					}
				}

				@Override
				public void itemsetRemoved(ItemSetRemovedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//System.err.println(event.getItemSet()+" eval:"+event.getItemSet().getEval());
					if(!macroChangeEncountered) {
						latestIFPCount++;
					}
				}

				@Override
				public void itemsetUpdated(ItemSetUpdatedEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//System.out.println(event.getItemSet()+" eval:"+event.getItemSet().getEval());
					if(!macroChangeEncountered) {
						latestFPCount++;
					}
				}

				@Override
				public void addedItemsetRegistered(
					ItemSetAddedRegisteredEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//System.out.println(event.getItemSet()+" eval:"+event.getItemSet().getEval());
				}

				@Override
				public void removedItemsetRegistered(
					ItemSetRemovedRegisteredEvent<LabeledEdge, Pair<Set<Integer>>> event) {
					//System.err.println(event.getItemSet()+" eval:"+event.getItemSet().getEval());
				}

			}
		);

		//we execute the detection algorithm
		detector.execute();

	}




	/**
	 * Application entry point, in this static method
	 * we perform command-line argument parsing and validation.
	 * If validation succeed, we launch the detector on the designated
	 * data stream.
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final KarmaArgs arguments = new KarmaArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		final Validator validator = factory.getValidator();

		try {
			argsParser.parseArgument(args);
			Set<ConstraintViolation<KarmaArgs>> violations = validator.validate(arguments);
			if(violations.isEmpty()) {
				final KarmaApplication application = new KarmaApplication(arguments);
				application.run();
			}else {
				for(ConstraintViolation<KarmaArgs> violation : violations) {
					System.err.println("invalid argument: "+violation.getPropertyPath()+" "+violation.getMessage());
				}
			}
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);	
		} catch (XMLStreamException | FactoryConfigurationError | IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
