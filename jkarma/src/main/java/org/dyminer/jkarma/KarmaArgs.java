package org.dyminer.jkarma;

import java.io.File;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.kohsuke.args4j.Option;

public class KarmaArgs {
	
	@Option(name="-i", required=true, usage="path to a valid CSV input file")
	private File streamSource;
	
	@Option(name="-o", required=true, usage="path to the file that will contain the results")
	private File outputFile;
	
	@Option(name="-t", required=true, usage="pattern maximum length (greater or equal than 1)")
	@Min(1)
	private int maximumDepth;
	
	@Option(name="-bts", required=true, usage="initialization block size (greater or equal than 1)")
	@Min(1)
	private int batchSize;
	
	@Option(name="-bs", required=true, usage="fixed block size (greater or equal than 1)")
	@Min(1)
	private int blockSize;
	
	@Option(name="-minSup", required=true, usage="minimum support threshold (between 0 and 1)")
	@Min(0) @Max(1)
	private float minimumSupport;
	
	@Option(name="-minMc", required=true, usage="minimum macroscopic change threshold (between 0 and 1)")
	@Min(0) @Max(1)
	private float minimumMacrochange;
	
	@Option(name="-minGr", required=true, usage="minimum microscopic change/growth-rate threshold (greater or equal than 1)")
	@Min(1)
	private float minimumGrowthRate;
	
	@Option(name="-c", usage="will use closed pattern when searching microscopic changes")
	private boolean willMineClosed = false;
	
	@Option(name="-invlin", forbids={"-uniform","-exp"}, depends={"-theta"}, 
			usage="will use an inverse linear fading function to dampen the minMc")
	private boolean willUseLinearFader = false;
	
	@Option(name="-exp", forbids={"-uniform","-invlin"}, depends={"-theta"},
			usage="will use an exponential fading function to dampen the minMc")
	private boolean willUseExponentialFader = false;
	
	@Option(name="-uniform", forbids={"-theta, -exp","-invlin"}, usage="will use a uniform fading function to dampen the minMc")
	private boolean willUseUniformFader = true;
	
	@Option(name="-theta", usage="dampen factor for inverse linear/exponential fading function")
	@Min(0)
	private float theta;
	

	public File getStreamSource() {
		return streamSource;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public int getMaximumDepth() {
		return maximumDepth;
	}

	public int getBatchSize() {
		return batchSize;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public float getMinimumSupport() {
		return minimumSupport;
	}

	public float getMinimumMacrochange() {
		return minimumMacrochange;
	}

	public float getMinimumGrowthRate() {
		return minimumGrowthRate;
	}
	
	public boolean isMiningClosed() {
		return willMineClosed;
	}
	
	public boolean isUsingLinearFader() {
		return willUseLinearFader;
	}

	public boolean isUsingExponentialFader() {
		return willUseExponentialFader;
	}

	public boolean isUsingUniformFader() {
		return willUseUniformFader;
	}

	public float getTheta() {
		return theta;
	}

}
