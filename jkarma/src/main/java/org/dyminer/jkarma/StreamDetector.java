package org.dyminer.jkarma;

import java.util.stream.Stream;

import org.dyminer.jkarma.events.ChangeDetectorEventListener;
import org.dyminer.jkarma.events.DataStreamEventListener;
import org.dyminer.model.Transaction;

import com.google.common.eventbus.EventBus;


/**
 * A base class for a detector over data streams.
 * A detector is an agent that observe incoming data from the stream
 * in search for relevant events to be notified.
 * @author Angelo Impedovo
 *
 * @param <A> the transactional type of object in the stream
 */
public abstract class StreamDetector<A extends Transaction<?>> {
	
	protected EventBus eventBus;
	protected Stream<? extends A> dataStream;
	
	protected StreamDetector(Stream<? extends A> dataStream) {
		if(dataStream==null) {
			throw new NullPointerException();
		}
		
		this.eventBus = new EventBus();
		this.dataStream = dataStream;
	}
	
	public void registerListener(ChangeDetectorEventListener eventListener) {
		this.eventBus.register(eventListener);
	}
	
	public void unregisterListener(ChangeDetectorEventListener eventListener) {
		this.eventBus.unregister(eventListener);
	}
	
	public void registerListener(DataStreamEventListener<A> eventListener) {
		this.eventBus.register(eventListener);
	}
	
	public void unregisterListener(DataStreamEventListener<A> eventListener) {
		this.eventBus.unregister(eventListener);
	}
	
	public abstract void execute();

}
