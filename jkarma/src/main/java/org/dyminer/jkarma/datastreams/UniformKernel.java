package org.dyminer.jkarma.datastreams;

public class UniformKernel extends FadingFunction {

	public UniformKernel(Number baseline) {
		super(baseline, 0);
	}

	@Override
	public Number apply(Integer t) {
		return super.getBaseValue();
	}

}
