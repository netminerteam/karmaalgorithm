package org.dyminer.jkarma.datastreams;

import java.time.Instant;

import org.dyminer.model.Transaction;

public class TimeWindow{
	
	private Instant firstInstant;
	private Instant lastInstant;
	
	public TimeWindow(Block<? extends Transaction<?>> firstBlock) {
		if(firstBlock==null) {
			throw new NullPointerException();
		}
		
		this.firstInstant = firstBlock.getFirstInstant();
		this.lastInstant = firstBlock.getLastInstant();
	}
	
	public Instant getFirstInstant() {
		return this.firstInstant;
	}
	
	public Instant getLastInstant() {
		return this.lastInstant;
	}
	
	public void expand(Block<? extends Transaction<?>> block) {
		this.lastInstant = block.getLastInstant();
	}
	
	@Override
	public String toString() {
		return this.getFirstInstant()+", "+this.getLastInstant();
	}

}
