package org.dyminer.jkarma.datastreams;

import java.util.function.Function;


/**
 * An abstract class that should be used whenever there is a need
 * to use a numeric fading factor over a sequence of elements.
 * @author Angelo Impedovo
 *
 * @param <A>
 */
public abstract class FadingFunction implements Function<Integer, Number> {
	
	private Number valueToFade;
	private Number fadingRate;
	
	public FadingFunction(Number baseline, Number theta) {
		if(baseline==null || theta == null) {
			throw new NullPointerException();
		}
		this.valueToFade = baseline;
		this.fadingRate = theta;
	}

	public Number getBaseValue() {
		return this.valueToFade;
	}
	
	public Number getFadingRate() {
		return this.fadingRate;
	}
}
