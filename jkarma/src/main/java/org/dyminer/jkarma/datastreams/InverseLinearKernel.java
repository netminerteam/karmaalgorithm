package org.dyminer.jkarma.datastreams;

public class InverseLinearKernel extends FadingFunction {

	private Integer factor;
	
	public InverseLinearKernel(Number baseline, Number theta) {
		this(baseline, 1, theta);
	}
	
	public InverseLinearKernel(Number baseline, int factor, Number theta) {
		super(baseline, theta);
		this.factor = factor;
	}

	@Override
	public Number apply(Integer t) {
		double result;
		double theta = super.getFadingRate().doubleValue();
		double baseValue = super.getBaseValue().doubleValue();
		
		if(t == 0) {
			result = baseValue;
		}else {
			double factor = (this.factor.floatValue()/(t+1)) * theta;
			result = baseValue * factor;
		}
		
		return result;
	}

}
