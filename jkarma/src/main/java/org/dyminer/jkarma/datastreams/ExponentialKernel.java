package org.dyminer.jkarma.datastreams;

public class ExponentialKernel extends FadingFunction {
	
	public ExponentialKernel(Number baseline, Number theta) {
		super(baseline, theta);
	}

	
	@Override
	public Number apply(Integer t) {
		float result;
		float theta = super.getFadingRate().floatValue();
		float baseValue = super.getBaseValue().floatValue();
		
		if(t == 0) {
			result = baseValue;
		}else {
			float factor = ((float)Math.pow(1-theta, t-1)) * theta;
			result = baseValue * factor;
		}
		
		return result;
	}

}
