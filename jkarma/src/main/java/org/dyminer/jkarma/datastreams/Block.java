package org.dyminer.jkarma.datastreams;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;

import org.dyminer.model.Transaction;

public final class Block<A extends Transaction<?>> implements Iterable<A> {
	
	private ArrayList<A> transactions;
	private int capacity;
	
	public Block(int capacity) {
		this.transactions = new ArrayList<A>(capacity);
		this.capacity = capacity;
	}
	
	public Instant getFirstInstant() {
		Instant result = null;
		if(!this.isEmpty()) {
			A firstTransaction = this.getFirstTransaction();
			result = firstTransaction.getTimestamp();
		}
		return result;
	}
	
	public Instant getLastInstant() {
		Instant result = null;
		if(!this.isEmpty()) {
			A lastTransaction = this.getLastTransaction();
			result = lastTransaction.getTimestamp();
		}
		return result;
	}
	
	public A getFirstTransaction(){
		A transaction = null;
		if(!this.isEmpty()) {
			transaction = this.transactions.get(0);
		}
		return transaction;
	}
	
	public A getLastTransaction(){
		A transaction = null;
		if(!this.isEmpty()) {
			transaction = (A) this.transactions.get(this.getCount()-1);
		}
		return transaction;
	}
	
	public int getCount() {
		return this.transactions.size();
	}
	
	public int getCapacity() {
		return this.capacity;
	}
	
	public boolean isEmpty() {
		return this.getCount()==0;
	}
	
	public boolean isFull() {
		return this.getCount()==this.getCapacity();
	}
	
	public boolean addTransaction(A transaction) {
		boolean result = false;
		if(!this.isFull()) {
			this.transactions.add(transaction);
			result = true;
		}
		return result;
	}

	@Override
	public Iterator<A> iterator() {
		return this.transactions.iterator();
	}
	
	@Override
	public String toString() {
		return this.getFirstInstant()+", "+this.getLastInstant();
	}
	
}
