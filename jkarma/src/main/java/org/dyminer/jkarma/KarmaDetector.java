package org.dyminer.jkarma;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.FadingFunction;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.jkarma.events.ChangeDetectedEvent;
import org.dyminer.jkarma.events.ChangeNotDetectedEvent;
import org.dyminer.jkarma.events.DataStreamExhaustedEvent;
import org.dyminer.jkarma.events.DataStreamStartedEvent;
import org.dyminer.jkarma.events.DetectionCompletedEvent;
import org.dyminer.jkarma.events.DetectionStartedEvent;
import org.dyminer.jkarma.events.KarmaEventListener;
import org.dyminer.jkarma.events.LatticeUpdateCompletedEvent;
import org.dyminer.jkarma.events.LatticeUpdateStartedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeAcceptedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeDetectionCompletedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeDetectionStartedEvent;
import org.dyminer.jkarma.events.MicroscopicChangeNotAcceptedEvent;
import org.dyminer.jkarma.events.TransactionArrivedEvent;
import org.dyminer.jkarma.events.TransactionBlockArrivedEvent;
import org.dyminer.jminer.events.ItemSetAddedEvent;
import org.dyminer.jminer.events.ItemSetAddedRegisteredEvent;
import org.dyminer.jminer.events.ItemSetEventListener;
import org.dyminer.jminer.events.ItemSetRemovedEvent;
import org.dyminer.jminer.events.ItemSetRemovedRegisteredEvent;
import org.dyminer.jminer.events.ItemSetUpdatedEvent;
import org.dyminer.jminer.interfaces.ItemSet;
import org.dyminer.jminer.interfaces.Lattice;
import org.dyminer.jminer.joiners.FrequencyEvaluator;
import org.dyminer.jminer.joiners.Joiner;
import org.dyminer.jminer.providers.VerticalDataProvider;
import org.dyminer.jminer.structures.IncrementalStrategy;
import org.dyminer.jminer.structures.Pair;
import org.dyminer.jminer.structures.Strategies;
import org.dyminer.jminer.structures.TransactionException;
import org.dyminer.model.Transaction;


public class KarmaDetector<A extends Transaction<B>, B extends Comparable<B>, C> extends StreamDetector<A>{
	
	private KarmaArgs arguments;
	private VerticalDataProvider<B, C> provider;
	private IncrementalStrategy<B, C> miningStrategy;
	private FrequencyEvaluator<C> evaluator;
	private Lattice<ItemSet<B,Pair<C>>> lattice;
	private FadingFunction fader;
	
	private boolean willNotifyEvents; 
	
	protected KarmaDetector(KarmaArgs arguments, Stream<A> source, 
			Joiner<B> joiner, VerticalDataProvider<B, C> provider, 
			FrequencyEvaluator<C> evaluator, FadingFunction fader) {
		super(source);
		if(arguments==null || source==null || provider==null ||fader==null) {
			throw new NullPointerException();
		}
		
		//initialize some context variables
		this.arguments = arguments;
		this.provider = provider;
		this.evaluator = evaluator;
		this.fader = fader;
		this.willNotifyEvents = true;
		this.evaluator.setTransactionCount(0);
		
		//initialize the mining strategy
		this.miningStrategy = Strategies.onlineDepthFirst(
			joiner, this.provider, evaluator, this.arguments.getMaximumDepth()
		);
		this.miningStrategy.registerListener(
			new ItemSetEventListener<B,Pair<C>>(){
				
				@Override
				public void itemsetAdded(ItemSetAddedEvent<B, Pair<C>> event) {
					if(willNotifyEvents) {
						//we forward only the relevant events to the listener
						eventBus.post(event);
					}
				}
	
				@Override
				public void itemsetRemoved(ItemSetRemovedEvent<B, Pair<C>> event) {
					if(willNotifyEvents) {
						//we forward only the relevant events to the listener
						eventBus.post(event);
					}
				}
	
				@Override
				public void itemsetUpdated(ItemSetUpdatedEvent<B, Pair<C>> event) {
					if(willNotifyEvents) {
						//we forward only the relevant events to the listener
						eventBus.post(event);
					}
				}
	
				@Override
				public void addedItemsetRegistered(ItemSetAddedRegisteredEvent<B, Pair<C>> event) {
					if(willNotifyEvents) {
						//we forward only the relevant events to the listener
						eventBus.post(event);
					}
				}
	
				@Override
				public void removedItemsetRegistered(ItemSetRemovedRegisteredEvent<B, Pair<C>> event) {
					if(willNotifyEvents) {
						//we forward only the relevant events to the listener
						eventBus.post(event);
					}
				}
			}
		);
	}



	/**
	 * Karma algorithm entry point.
	 * This method consumes the data stream provided to the constructor and
	 * block-wise searches for macroscopic and microscopic changes.
	 */
	@Override
	public void execute() {
		//we throw a start event
		this.eventBus.post(new DataStreamStartedEvent<A>());
		
		//and then we consume the stream
		StreamConsumer consumer = new StreamConsumer();
		this.dataStream.sequential()
			.forEach(consumer);
		
		//finally we throw an end event
		this.eventBus.post(new DataStreamExhaustedEvent<A>());
	}
	
	
	
	/**
	 * Register an event listener designed to listen to karma related events
	 * such as the identification of microscopic changes.
	 * @param eventListener
	 */
	public void registerListener(KarmaEventListener<B, ? extends Pair<C>> eventListener) {
		this.eventBus.register(eventListener);
	}
	
	
	
	/**
	 * Unregister the event listener that listen to events strictly related with karma
	 * @param eventListener
	 */
	public void unregisterListener(KarmaEventListener<B, ? extends Pair<C>> eventListener) {
		this.eventBus.unregister(eventListener);
	}
	
	
	
	/**
	 * Register an event listener designed to listen to events related to the
	 * designated underlying itemset mining strategy.
	 * @param eventListener
	 */
	public void registerListener(ItemSetEventListener<B, ? extends Pair<C>> eventListener) {
		this.eventBus.register(eventListener);
	}
	
	
	
	/**
	 * Unregister the event listener that listen to events related to the mining strategy.
	 * @param eventListener
	 */
	public void unregisterListener(ItemSetEventListener<B, ? extends Pair<C>> eventListener) {
		this.eventBus.unregister(eventListener);
	}
	
	
	
	/**
	 * A private stream consumer.
	 * The consumer is the main component that contributes to the
	 * implementation of the karma algorithm.
	 * The consumer consumes the stream searching for target objects,
	 * furthermore the consumer manages the state of time windows.
	 * 
	 * @author Angelo Impedovo
	 */
	private class StreamConsumer implements Consumer<A>{
		
		private int blockIndex = -1;
		private boolean firstBlockCompleted = false;
		private TimeWindow currentLandmark = null;
		private Block<A> currentBlock;

		@Override
		public void accept(A transaction) {
			if(currentBlock == null) {
				//first object arrived, we initialize a block of default batch size
				this.currentBlock = new Block<A>(arguments.getBatchSize());
				this.currentLandmark = new TimeWindow(this.currentBlock);
			}
			
			//we add the transaction to the block and to the provider
			this.currentBlock.addTransaction(transaction);
			eventBus.post(new TransactionArrivedEvent<A>(transaction));
			
			//we add the corresponding items as generators of the lattice;
			provider.accept(transaction);
			for(B item : transaction) {
				miningStrategy.setGenerator(item);
			}
			
			//we check if we filled the current block;
			if(this.currentBlock.isFull()) {
				//we raise an event
				eventBus.post(new TransactionBlockArrivedEvent<A>(this.currentBlock));
				evaluator.setTransactionCount(evaluator.getTransactionCount()+this.currentBlock.getCount());
				
				//we should inspect for changes here
				try {
					//we raise an event of lattice update start
					eventBus.post(new LatticeUpdateStartedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
					//we update the lattice of pattern
					lattice = miningStrategy.execute();
					//we raise an event of lattice update completed
					eventBus.post(new LatticeUpdateCompletedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
					
					//change detection only makes sense starting from the second block
					if(this.firstBlockCompleted) {
						//we raise an event on change detection start
						eventBus.post(new DetectionStartedEvent(this.currentLandmark, this.currentBlock));
						//we dampen the current minMc threshold
						float dampedMinMc = fader.apply(this.blockIndex).floatValue();
						//we compute the macroscopic change intensity
						float detectedMacroChange = this.getMacroscopicChange();
						//so we can test it against the minimum threshold provided by the user
						boolean changeDetected = (detectedMacroChange > dampedMinMc);
						
						if(changeDetected) {
							//we raise an event of macroscopic change found
							eventBus.post(new ChangeDetectedEvent(this.currentLandmark, this.currentBlock, detectedMacroChange));
							
							//we raise an event of microscopic detection start
							eventBus.post(new MicroscopicChangeDetectionStartedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
							//then we check for microscopic changes
							this.inspectMicroscopicChanges();
							//we raise an event of microscopic detection completed
							eventBus.post(new MicroscopicChangeDetectionCompletedEvent<B,Pair<C>>(this.currentLandmark, this.currentBlock));
							
							//there is no need to notify the events from this point on
							willNotifyEvents = false;
							
							//we commit any pending modifications
							//System.err.println(lattice.getCount());
							miningStrategy.commit();
							//System.err.println(lattice.getCount());
							
							//we set the transaction count to that of a single block...
							evaluator.setTransactionCount(arguments.getBlockSize());
							provider.forgetCached();
							
							//...then we forget everything in the past (and we commit)...
							miningStrategy.forget();
							miningStrategy.commit();
							
							//and then we compute any missing pattern in the latest block (and, again, we commit)
							miningStrategy.execute();
							miningStrategy.commit();
							provider.flatten();
							
							//then we re-enable the events
							willNotifyEvents = true;
							this.currentLandmark = new TimeWindow(this.currentBlock);
							this.blockIndex = -1;
						}else{
							//we raise an event of macroscopic change not found
							eventBus.post(new ChangeNotDetectedEvent(this.currentLandmark, this.currentBlock, detectedMacroChange));
							
							//and, finally we commit and we flatten the provider
							miningStrategy.commit();
							provider.flatten();
							this.currentLandmark.expand(this.currentBlock);
						}
						
						//we raise an event on change detection end
						eventBus.post(new DetectionCompletedEvent(this.currentLandmark, this.currentBlock));
					}else{
						//we commit any pending modifications of the lattice since we are only building
						//the lattice on the first block;
						miningStrategy.commit();
						provider.flatten();
						this.currentLandmark = new TimeWindow(this.currentBlock);
					}
				} catch (TransactionException e) {
					System.err.println(e.getMessage());
					e.printStackTrace();
				}
				
				//we set the first block flag to true
				if(!this.firstBlockCompleted) {
					this.firstBlockCompleted = true;
				}
				blockIndex++;
				
				//then we should set a new current empty block
				this.currentBlock = new Block<A>(arguments.getBlockSize());
			}
		}
		
		
		
		private void debugLattice() {
			for(ItemSet<B, Pair<C>> a : lattice) {
				if(a.getSuffix()!=null) {
					Pair<C> eval = a.getEval();
					C aggr = evaluator.apply(eval.getAggregate(), eval.getIncrement());
					System.out.println(a+" eval:"+evaluator.getAbsoluteFrequency(aggr)+" rel:"+evaluator.getRelativeFrequency(aggr));
				}
			}
		}
		
		
		
		/**
		 * The function is used to quantify any microscopic change
		 * when a macroscopic change has been found.
		 */
		private void inspectMicroscopicChanges() {
			List<ItemSet<B,Pair<C>>> itemsetsAdded = miningStrategy.getRegisteredAsAddedIterator();
			List<ItemSet<B,Pair<C>>> itemsetsRemoved = miningStrategy.getRegisteredAsRemovedIterator();
			
			//we inspect the subspace of patterns added to the lattice
			for(ItemSet<B,Pair<C>> pattern : itemsetsAdded) {
				//we inspect the emergency of such patterns (and the closure, just in case)
				float pastFreq = this.getPastRelativeSupport(pattern);
				float newFreq = this.getRelativeSupport(pattern);
				float growthRate = newFreq/pastFreq;
				boolean isChange = (growthRate >= arguments.getMinimumGrowthRate());
				
				//we verify that the pattern is closed (only if the algorithm will mine closed pattern)
				boolean isClosed = false;
				if(arguments.isMiningClosed()){
					if(isChange){
						isClosed = this.isClosed(pattern);
					}
				}
				
				//we finally check for emergency
				if((arguments.isMiningClosed() && isClosed) || !arguments.isMiningClosed()){
					if(isChange) {
						eventBus.post(new MicroscopicChangeAcceptedEvent<B,Pair<C>>(
							currentLandmark, currentBlock, pattern, pastFreq, newFreq, growthRate
						));
					}else {
						eventBus.post(new MicroscopicChangeNotAcceptedEvent<B,Pair<C>>(
							currentLandmark, currentBlock, pattern, pastFreq, newFreq, growthRate
						));
					}
				}
			}
			
			//we inspect the subspace of patterns removed from the lattice
			for(ItemSet<B,Pair<C>> pattern : itemsetsRemoved) {
				//we inspect the inverse emergency of such patterns (and the closure, just in case)
				float pastFreq = this.getPastRelativeSupport(pattern);
				float newFreq = this.getRelativeSupport(pattern);
				float growthRate = pastFreq/newFreq;
				boolean isChange = (growthRate >= arguments.getMinimumGrowthRate());
				
				//we verify that the pattern is closed (only if the algorithm will mine closed pattern)
				boolean isClosed = false;
				if(arguments.isMiningClosed()){
					if(isChange){
						isClosed = this.isClosed(pattern);
					}
				}
				
				//we finally check for emergency
				if((arguments.isMiningClosed() && isClosed) || !arguments.isMiningClosed()){
					if(isChange) {
						eventBus.post(new MicroscopicChangeAcceptedEvent<B,Pair<C>>(
							currentLandmark, currentBlock, pattern, pastFreq, newFreq, growthRate
						));
					}else {
						eventBus.post(new MicroscopicChangeNotAcceptedEvent<B,Pair<C>>(
							currentLandmark, currentBlock, pattern, pastFreq, newFreq, growthRate
						));
					}
				}
			}
		}
	
		
		/**
		 * Convenience function that returns the relative support (according to the
		 * evaluator) of a pattern with respect to old transactions (discarding the latest
		 * arrived block).
		 * 
		 * @param pattern
		 * @return
		 */
		private float getPastRelativeSupport(ItemSet<B,Pair<C>> pattern) {
			int transactionCount = evaluator.getTransactionCount()-arguments.getBlockSize();
			int absFreq = evaluator.getAbsoluteFrequency(pattern.getEval().getAggregate());
			float support = (float)absFreq/(float)transactionCount;
			return support;
		}
		
		
		/**
		 * Convenience function that returns the relative support (according to the
		 * evaluator) of a pattern to the latest data point.
		 * 
		 * @param pattern
		 * @return
		 */
		private float getRelativeSupport(ItemSet<B,Pair<C>> pattern) {
			int transactionCount = evaluator.getTransactionCount();
			int absFreq = evaluator.getAbsoluteFrequency(pattern.getEval().getAggregate())+
					evaluator.getAbsoluteFrequency(pattern.getEval().getIncrement());
			float support = (float)absFreq/(float)transactionCount;
			return support;
		}
		
		
		
		/**
		 * The function is used to quantify the amount of macroscopic change
		 * when a new block of transaction arrives from the stream.
		 * @return
		 */
		private float getMacroscopicChange() {
			int newFrequent = miningStrategy.getRegisteredAsAddedIterator().size();
			int newInfrequent = miningStrategy.getRegisteredAsRemovedIterator().size();
			int oldFrequent = lattice.getCount() - 1;
			float numerator = (float)(newFrequent+newInfrequent);
			float denominator = (float)(oldFrequent+newFrequent); //=(oldF+newF) = (nowF+newIF)
			float macroChange = 0;
			if(denominator!=0){
				macroChange = numerator/denominator;
			}
			
			return macroChange;
		}
		
		
		
		/**
		 * The function checks if a pattern is closed.
		 * A pattern is closed if none of its superset has the same evaluation.
		 * @param pattern
		 * @return
		 */
		private boolean isClosed(ItemSet<B,Pair<C>> pattern){
			boolean closed = true;
			Pair<C> evaluation = pattern.getEval();
			for(ItemSet<B,Pair<C>> superset : lattice.getLowerElements(pattern)) {
				Pair<C> superSetEvaluation = superset.getEval();
				if(evaluation.equals(superSetEvaluation)) {
					closed = false;
					break;
				}
			}
			return closed;
		}
		
	} 
}
