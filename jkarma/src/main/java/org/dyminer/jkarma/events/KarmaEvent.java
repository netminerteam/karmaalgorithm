package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.model.Transaction;

public abstract class KarmaEvent<A extends Comparable<A>, B> {
	
	public TimeWindow landmarkWindow;
	public Block<? extends Transaction<A>> latestBlock;
	
	public KarmaEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock) {
		if(landmark==null || latestBlock==null) {
			throw new NullPointerException();
		}
		this.landmarkWindow = landmark;
		this.latestBlock = latestBlock;
	}

	public TimeWindow getLandmarkWindow() {
		return landmarkWindow;
	}

	public Block<? extends Transaction<A>> getLatestBlock() {
		return latestBlock;
	}	

}