package org.dyminer.jkarma.events;

import com.google.common.eventbus.Subscribe;

public interface ChangeDetectorEventListener {
	
	@Subscribe
	public void detectionStarted(DetectionStartedEvent event);
	
	@Subscribe
	public void detectionCompleted(DetectionCompletedEvent event);
	
	@Subscribe
	public void changeDetected(ChangeDetectedEvent event);
	
	@Subscribe
	public void changeNotDetected(ChangeNotDetectedEvent event);
	
}
