package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;

public abstract class ChangeDetectorEvent {
	
	public TimeWindow landmarkWindow;
	public Block<?> latestBlock;
	
	public ChangeDetectorEvent(TimeWindow landmark, Block<?> latestBlock) {
		if(landmark==null || latestBlock==null) {
			throw new NullPointerException();
		}
		this.landmarkWindow = landmark;
		this.latestBlock = latestBlock;
	}

	public TimeWindow getLandmarkWindow() {
		return landmarkWindow;
	}

	public Block<?> getLatestBlock() {
		return latestBlock;
	}

}
