package org.dyminer.jkarma.events;

import com.google.common.eventbus.Subscribe;

/**
 * A custom Event Listener specific to listen events that are strictly related
 * with the Karma algorithm (and not related to other types of detector).
 * @author Angelo Impedovo
 *
 * @param <A>
 * @param <B>
 */
public interface KarmaEventListener<A extends Comparable<A>, B> {
	
	@Subscribe
	public void latticeUpdateStarted(LatticeUpdateStartedEvent<A,B> event);
	
	@Subscribe
	public void latticeUpdateCompleted(LatticeUpdateCompletedEvent<A,B> event);

	@Subscribe
	public void microscopicChangeDetectionStarted(MicroscopicChangeDetectionStartedEvent<A,B> event);
	
	@Subscribe
	public void microscopicChangeAccepted(MicroscopicChangeAcceptedEvent<A,B> event);
	
	@Subscribe
	public void microscopicChangeNotAccepted(MicroscopicChangeNotAcceptedEvent<A,B> event);
	
	@Subscribe
	public void microscopicChangeDetectionCompleted(MicroscopicChangeDetectionCompletedEvent<A,B> event);

}
