package org.dyminer.jkarma.events;

import org.dyminer.model.Transaction;

import com.google.common.eventbus.Subscribe;

/**
 * A custom Event Listener to listen to events that are strictly related
 * to data streams management.
 * @author Angelo Impedovo
 *
 * @param <A>
 */
public interface DataStreamEventListener<A extends Transaction<?>> {
	
	@Subscribe
	public void dataStreamStarted(DataStreamStartedEvent<A> event);
	
	@Subscribe
	public void dataStreamExhausted(DataStreamExhaustedEvent<A> event);
	
	@Subscribe
	public void transactionArrived(TransactionArrivedEvent<A> event);
	
	@Subscribe
	public void transactionBlockArrived(TransactionBlockArrivedEvent<A> event);
	
}
