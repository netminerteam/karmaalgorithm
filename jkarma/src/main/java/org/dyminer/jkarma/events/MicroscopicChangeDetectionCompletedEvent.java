package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.model.Transaction;

public class MicroscopicChangeDetectionCompletedEvent<A extends Comparable<A>, B> extends KarmaEvent<A,B> {

	public MicroscopicChangeDetectionCompletedEvent(TimeWindow landmark, Block<? extends Transaction<A>> currentBlock) {
		super(landmark, currentBlock);
	}

}
