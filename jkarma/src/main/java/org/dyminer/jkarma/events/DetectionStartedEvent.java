package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;

public class DetectionStartedEvent extends ChangeDetectorEvent {

	public DetectionStartedEvent(TimeWindow landmark, Block<?> latestBlock) {
		super(landmark, latestBlock);
	}
	
}
