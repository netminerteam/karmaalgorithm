package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.model.Transaction;

public class LatticeUpdateStartedEvent<A extends Comparable<A>, B> extends KarmaEvent<A,B> {

	public LatticeUpdateStartedEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock) {
		super(landmark, latestBlock);
	}

}
