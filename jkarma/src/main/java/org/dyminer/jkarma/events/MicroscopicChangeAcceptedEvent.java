package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.jminer.interfaces.ItemSet;
import org.dyminer.model.Transaction;

public class MicroscopicChangeAcceptedEvent<A extends Comparable<A>, B> extends KarmaEvent<A,B> {
	
	private ItemSet<A,B> pattern;
	float growthRate;
	private float pastRelativeFrequency;
	private float updatedRelativeFrequency;

	public MicroscopicChangeAcceptedEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock,
			ItemSet<A, B> pattern, float pastFreq, float newFreq, float growthRate) {
		super(landmark, latestBlock);
		if(pattern==null) {
			throw new NullPointerException();
		}
		this.pattern = pattern;
		this.pastRelativeFrequency = pastFreq;
		this.updatedRelativeFrequency = newFreq;
		this.growthRate = growthRate;
	}

	public ItemSet<A, B> getPattern() {
		return pattern;
	}

	public float getGrowthRate() {
		return growthRate;
	}
	
	public float getPastRelativeFrequency() {
		return pastRelativeFrequency;
	}

	public float getUpdatedRelativeFrequency() {
		return updatedRelativeFrequency;
	}

}
