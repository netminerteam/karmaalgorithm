package org.dyminer.jkarma.events;

import org.dyminer.model.Transaction;

public class TransactionArrivedEvent<A extends Transaction<?>> extends DataStreamEvent<A>{
	
	private A transaction;
	
	public TransactionArrivedEvent(A transaction) {
		super();
		this.transaction = transaction;
	}
	
	public A getTransaction() {
		return this.transaction;
	}

}
