package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;

public class DetectionCompletedEvent extends ChangeDetectorEvent{

	public DetectionCompletedEvent(TimeWindow landmark, Block<?> latestBlock) {
		super(landmark, latestBlock);
	}

}
