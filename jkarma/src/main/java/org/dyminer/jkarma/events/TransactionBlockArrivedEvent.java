package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.model.Transaction;

public class TransactionBlockArrivedEvent<A extends Transaction<?>> extends DataStreamEvent<A> {
	
	private Block<A> block;
	
	public TransactionBlockArrivedEvent(Block<A> incomingBlock) {
		super();
		this.block = incomingBlock;
	}
	
	public Block<A> getBlock(){
		return this.block;
	}

}
