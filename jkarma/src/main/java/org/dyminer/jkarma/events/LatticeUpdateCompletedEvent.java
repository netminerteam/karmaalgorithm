package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;
import org.dyminer.model.Transaction;

public class LatticeUpdateCompletedEvent<A extends Comparable<A>, B> extends KarmaEvent<A,B> {

	public LatticeUpdateCompletedEvent(TimeWindow landmark, Block<? extends Transaction<A>> latestBlock) {
		super(landmark, latestBlock);
	}

}
