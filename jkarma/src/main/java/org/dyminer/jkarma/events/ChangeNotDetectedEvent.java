package org.dyminer.jkarma.events;

import org.dyminer.jkarma.datastreams.Block;
import org.dyminer.jkarma.datastreams.TimeWindow;

public class ChangeNotDetectedEvent extends ChangeDetectorEvent{

	private float changeAmount;
	
	public ChangeNotDetectedEvent(TimeWindow landmark, Block<?> latestBlock, float amount) {
		super(landmark, latestBlock);
		this.changeAmount = amount;
	}
	
	public float getChangeAmount() {
		return this.changeAmount;
	}
	
}
