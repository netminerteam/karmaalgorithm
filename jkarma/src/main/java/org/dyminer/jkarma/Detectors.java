package org.dyminer.jkarma;

import java.util.Set;
import java.util.stream.Stream;

import org.dyminer.jkarma.datastreams.FadingFunction;
import org.dyminer.jminer.joiners.FrequencyEvaluator;
import org.dyminer.jminer.joiners.Joiner;
import org.dyminer.jminer.providers.VerticalDataProvider;
import org.dyminer.model.LabeledEdge;
import org.dyminer.model.TemporalGraph;
import org.dyminer.model.Transaction;

public class Detectors {

	public static <A extends Transaction<B>, B extends Comparable<B>, C> StreamDetector<A>
	genericKarma(KarmaArgs args, Stream<A> dataStream,
			Joiner<B> joiner, FrequencyEvaluator<C> aggregator, FadingFunction fader){
		return new KarmaDetector<A,B,C>(args, dataStream, joiner, null, aggregator, fader);
	}
	
	public static KarmaDetector<TemporalGraph, LabeledEdge, Set<Integer>>
	subgraphKarma(KarmaArgs args, Stream<TemporalGraph> dataStream, 
			Joiner<LabeledEdge> joiner, VerticalDataProvider<LabeledEdge, Set<Integer>> provider,
			FrequencyEvaluator<Set<Integer>> evaluator, FadingFunction fader){
		return new KarmaDetector<TemporalGraph, LabeledEdge, Set<Integer>>(
				args, dataStream, joiner, provider, evaluator, fader);
	}

}
